/**
 * @Author: harsha
 * @Date:   2017-12-03T13:59:20+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-03T14:00:51+05:30
 */

module.exports = {
  module:{
    rules:[
      {
      test: /\.js?$/,
      loader: 'babel-loader',
      exclude:'/node_modules/',
      options:{
        presets:[
          'react',
          'stage-0',
          ['env',{targets:{browsers:['last 2 versions']}}]
        ]
       }
      }
    ]
  }
}
