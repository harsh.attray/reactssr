/**
 * @Author: harsha
 * @Date:   2017-12-03T18:42:08+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-03T18:43:50+05:30
 */
import { combineReducers } from 'redux';
import usesReducer from './usersReducers';

export default combineReducers({
  users: usersReducers
})
