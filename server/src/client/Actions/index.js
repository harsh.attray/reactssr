/**
 * @Author: harsha
 * @Date:   2017-12-03T18:32:49+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-03T18:37:01+05:30
 */
import axios from 'axios';

export const FETCH_USERS = 'FETCH_USERS';

export const fetchUsers =  () => async dispatch => {
  const res = await axios.get('https://react-ssr-api.herokuapp.com/users')

  dispatch({
    type: FETCH_USERS,
    payload: res
  });
};
