/**
 * @Author: harsha
 * @Date:   2017-12-03T15:01:55+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-03T18:22:42+05:30
 */
import React from 'react';
import {renderToString} from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import Routes from '../client/routes';

export default (req,store) => {
  const content = renderToString(
    <Provider store={store}>
    <StaticRouter location={req.path} context={{}}>
      <Routes  />
    </StaticRouter>
    </Provider>
  );
  return (`
   <html>
    <head></head>
    <body>
      <div id="root">
      ${content}
      <script src="bundle.js"></script>
      </div>
    </body>
   </html>
  `);
}
