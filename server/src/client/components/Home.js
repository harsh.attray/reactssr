/**
 * @Author: harsha
 * @Date:   2017-12-02T13:44:23+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-02T20:25:37+05:30
 */
import React, { Component } from 'react';

const Home = () => {
  return(
    <div>
    <div> Home Component Rendered  Here</div>
    <button onClick={() => console.log('Pressed')}>Press This Button  generated from Server</button>
    </div>
  );
};

export default Home;
