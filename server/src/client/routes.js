/**
 * @Author: harsha
 * @Date:   2017-12-03T16:23:54+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-03T18:31:31+05:30
 */
import React from 'react';
import { Route } from 'react-router-dom';
import Home from './components/Home';

export default () => {
  return(
    <div>
      <Route exact path="/" component={ Home } />
    </div>
  );
};
