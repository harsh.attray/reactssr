/**
 * @Author: harsha
 * @Date:   2017-12-02T20:34:59+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-03T18:11:55+05:30
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import {createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import Routes from './routes';


const store = createStore(reducers,{},applyMiddleWare(thunk));

ReactDOM.hydrate(
  <Provider store = {store}>
    <BrowserRouter>
     <Routes />
    </BrowserRouter>
  </Provider>,
  document.querySelector('#root'));
