/**
 * @Author: harsha
 * @Date:   2017-12-03T18:37:49+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-03T18:41:52+05:30
 */
import {FETCH_USERS} from '../Actions/index';

export default (state=[],action) => {
  switch(action.type){
    case FETCH_USERS:
      return action.payload.data;
    default:
       return state;
  }
};
