/**
 * @Author: harsha
 * @Date:   2017-12-02T14:03:11+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-03T14:52:24+05:30
 */

const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base');
const webpackNodeExternals = require('webpack-node-externals');

  const config = {
  // Inform webpack that we are building a bundle for node and not browser
  target: 'node',
  //entrypoint into application
  entry:'./src/index.js',
  output:{
    //Tell webpack where the output file has to be placed
    filename: 'bundle.js',
    path: path.resolve(__dirname,'build')
  },
  externals: [webpackNodeExternals()]// anything that exists in the node modules folder will not be included in the server side bundle
  //Tell webpack to run babel on every file it runs through
};

module.exports = merge(baseConfig,config);
