/**
 * @Author: harsha
 * @Date:   2017-12-03T18:13:52+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-03T18:17:10+05:30
 */
import {createStore, applyMiddleWare} from 'redux';
import thunk from 'redux-thunk';

export default () =>{
  const store = createStore(reducers,{},applyMiddleWare(thunk));
  return store;
}
