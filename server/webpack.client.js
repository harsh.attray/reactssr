/**
 * @Author: harsha
 * @Date:   2017-12-02T20:31:54+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-03T14:10:01+05:30
 */
const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base');

const config = {
  entry: './src/client/client.js',
  output:{
    filename:'bundle.js',
    path: path.resolve(__dirname, 'public')
  }
}


module.exports = merge(baseConfig,config);
