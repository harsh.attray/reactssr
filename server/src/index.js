/**
 * @Author: harsha
 * @Date:   2017-12-02T13:34:02+05:30
 * @Last modified by:   harsha
 * @Last modified time: 2017-12-03T18:20:13+05:30
 */
//
//
// const express = require('express');
// const React = require('react');
// const renderToString = require('react-dom/server').renderToString;
// const Home = require('./client/components/Home').default;


import express from 'express';
// import React from 'react';
// import { renderToString } from 'react-dom/server';
// import Home from './client/components/Home';
import renderer from './helpers/renderer';
import createStore from './helpers/createStore';


//Root route handler.
const app = express();

//Treat the public directory as static
app.use(express.static('public'));
app.get('*',(req,res) => {
   const store = createStore();
   res.send(renderer(req,store));
});


app.listen('3000',() => {
  console.log('Active Server Deployed Again');
});
